#     Orientação a Objetos 2/2016

##    Aluno: Pedro Lucas Pinheiro de Souza
###   Matrícula: 13/0035581

####  Sistema de cadastro de recepção de um hospital

##### Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o terminal;
* Encontre o diretório raiz do projeto;
* Limpe os arquivos objeto:
	**$ make clean** 
* Compile o programa: 
	**$ make**
* Execute:
	**$ make run**
