BINFOLDER := ./bin/
INCFOLDER := ./inc/
SRCFOLDER := ./src/
OBJFOLDER := ./obj/

COMPILER := g++
FLAGS := -Wall -ansi -I$(INCFOLDER)

SRCFILES := $(wildcard src/*.cpp)

all:	 $(SRCFILES:src/%.cpp=obj/%.o)
		$(COMPILER) $(FLAGS) $(OBJFOLDER)*.o -o $(BINFOLDER)prog

obj/%.o:	src/%.cpp
				$(COMPILER) $(FLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -rf $(BINFOLDER)*
	rm -rf $(OBJFOLDER)*.o
	rm -rf $(INCFOLDER)*~
	rm -rf $(SRCFOLDER)*~
	rm -rf *~

run:
	$(BINFOLDER)prog

